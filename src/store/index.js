import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    page: 1,
    totalPages: 1
  },
  mutations: {
    setPage(state, page) {
      state.page = page
    },
    setTotalPages(state, totalPages) {
      state.totalPages = totalPages
    },
  },
  actions: {
  },
  modules: {
  },
  getters: {
    page: state => state.page,
    totalPages: state => state.totalPages

  }
})
